package br.com.zup.ApiPetShop.services;

import br.com.zup.ApiPetShop.dtos.DonoDTO;
import br.com.zup.ApiPetShop.exceptions.DonoDuplicadoException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DonoService {
    private List<DonoDTO> donos = new ArrayList<>();

    public DonoDTO adicionarDono(DonoDTO dono){
        verificarSeExisteDono(dono);

        donos.add(dono);
        return dono;
    }

    public void verificarSeExisteDono(DonoDTO dono){
        for (DonoDTO obj : donos){
            if (obj.getNome().equals(dono.getNome())){
                throw new DonoDuplicadoException("Dono já cadastrado");
            }
        }
    }

}
