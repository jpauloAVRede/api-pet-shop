package br.com.zup.ApiPetShop.services;

import br.com.zup.ApiPetShop.dtos.AnimalDTO;
import br.com.zup.ApiPetShop.dtos.PesquisaEspecieDTO;
import br.com.zup.ApiPetShop.exceptions.EmailNaoCadException;
import br.com.zup.ApiPetShop.exceptions.EspecieNaoCadastradaException;
import br.com.zup.ApiPetShop.exceptions.PetNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class PetShopService {
    @Autowired
    private DonoService donoService;
    private List<AnimalDTO> listaCadAnimal = new ArrayList<>();

    public void cadastarPet(AnimalDTO animalDTO) {
        //donoService.adicionarDono(animalDTO)
        listaCadAnimal.add(animalDTO);
    }

    public List<AnimalDTO> exibirLista(){
        return this.listaCadAnimal;
    }

    public List<AnimalDTO> exibirMesmaEspecie(String especie) {
        List<AnimalDTO> listaMesmaEspecie = new ArrayList<>();
        listaMesmaEspecie = null;
        for (AnimalDTO elemLista : this.listaCadAnimal) {
            if (elemLista.getEspecie().toString().equals(especie)){
                listaMesmaEspecie.add(elemLista);
            }
        }
        if (listaMesmaEspecie != null){
            return listaMesmaEspecie;
        }
        throw new EspecieNaoCadastradaException("Não há pet dessa especie cadastrada");
    }

    public AnimalDTO pesquisaAnimalPeloNome(String nome){
        AnimalDTO animalDTO = null;

        for (AnimalDTO elemLista : listaCadAnimal) {
            if (elemLista.getNome().equals(nome)){
                animalDTO = elemLista;
            }
        }

        if (animalDTO != null) {
            return animalDTO;
        }
        throw new PetNaoEncontradoException("Pet não cadastrado!!!");
    }

    public AnimalDTO pesquisaAnimalPeloDono(String email){
        AnimalDTO animalDTO = null;

        for (AnimalDTO elemLista : listaCadAnimal) {
            if (elemLista.getDonoDTO().getEmail().equals(email)){
                animalDTO = elemLista;
            }
        }

        if (animalDTO != null){
            return animalDTO;
        }
        throw new EmailNaoCadException("E-mail não pertence a nenhum dono!!!");
    }

    //atualizando pet
    private AnimalDTO atualizarAnimal(AnimalDTO objParaAtualizar, AnimalDTO objDaRequisicao){
        objDaRequisicao.setIdade(objDaRequisicao.getIdade());
        objParaAtualizar.setEspecie(objDaRequisicao.getEspecie());
        objParaAtualizar.setDonoDTO(objDaRequisicao.getDonoDTO());

        return objParaAtualizar;
    }

    public AnimalDTO atualizaOuCadastraAnimal(AnimalDTO animalDTO){

        try {
            AnimalDTO objtoDaLista = pesquisaAnimalPeloDono(animalDTO.getNome());
            objtoDaLista = atualizarAnimal(objtoDaLista, animalDTO);
            return objtoDaLista;
        } catch (PetNaoEncontradoException e){
            cadastarPet(animalDTO);
            return animalDTO;
        }
    }

}
