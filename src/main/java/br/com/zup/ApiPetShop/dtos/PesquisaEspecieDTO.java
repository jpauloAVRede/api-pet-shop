package br.com.zup.ApiPetShop.dtos;

public class PesquisaEspecieDTO {
    private String pesqEspecie;

    public PesquisaEspecieDTO() {}

    public String getPesqEspecie() {
        return pesqEspecie;
    }

    public void setPesqEspecie(String pesqEspecie) {
        this.pesqEspecie = pesqEspecie;
    }
}
