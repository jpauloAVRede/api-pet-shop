package br.com.zup.ApiPetShop.dtos;

import br.com.zup.ApiPetShop.emuns.Especie;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class AnimalDTO {

    private String nome;
    private int idade;
    private Especie especie;
    private DonoDTO donoDTO;

    public AnimalDTO() {}

    @Size(min = 2, message = "{validacao.nome}")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @NotNull (message = "{validacao.null}")
    @Min(value = 1, message = "{validacao.idade}")
    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @NotNull(message = "{validacao.null}")
    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    @Valid //deve-se colocar o @valid para validar os atributos da classe Dono
    @NotNull (message = "{validacao.null}")
    public DonoDTO getDonoDTO() {
        return donoDTO;
    }

    public void setDonoDTO(DonoDTO donoDTO) {
        this.donoDTO = donoDTO;
    }

}
