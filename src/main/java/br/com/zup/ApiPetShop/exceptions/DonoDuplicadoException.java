package br.com.zup.ApiPetShop.exceptions;

public class DonoDuplicadoException extends RuntimeException {
    private int statusCode = 400;

    public DonoDuplicadoException(String message) {
        super(message);
    }
}
