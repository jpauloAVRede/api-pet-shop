package br.com.zup.ApiPetShop.exceptions;

public class EmailNaoCadException extends RuntimeException{
    public EmailNaoCadException(String message) {
        super(message);
    }
}
