package br.com.zup.ApiPetShop.exceptions;

public class PetNaoEncontradoException extends RuntimeException{

    public PetNaoEncontradoException(String message){
        super(message);
    }

}
