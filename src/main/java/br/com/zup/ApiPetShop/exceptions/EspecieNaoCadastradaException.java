package br.com.zup.ApiPetShop.exceptions;

public class EspecieNaoCadastradaException extends RuntimeException{
    public EspecieNaoCadastradaException(String message) {
        super(message);
    }
}
