package br.com.zup.ApiPetShop.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControleAdivisor {

    @ExceptionHandler(MethodArgumentNotValidException.class) //para chegar nessa exception deve utilizar @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro manipularExceccoesDeValidacao(MethodArgumentNotValidException exception){
        List<FieldError> fieldErros = exception.getBindingResult().getFieldErrors(); //pegando todos os erros
        // convertendo a lista field em stream(maneira independente);
        //.map() - mapear os objetos dentro da lista;
        //lambda -cada objeto, será executado o proximo comando ( objeto -> sera executado)
        //.colect.tolist() - coletar e converter numa dada lista;
        List<Erro> erros = fieldErros.stream().map(objeto -> new Erro(objeto.getDefaultMessage())).collect(Collectors.toList());

        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(PetNaoEncontradoException.class)
    public MensagemDeErro manipularPetNaoEncontrato(PetNaoEncontradoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));

        return new MensagemDeErro(400, erros);
    }

    //exemplo de exceção customizada

    @ExceptionHandler(EspecieNaoCadastradaException.class)
    public MensagemDeErro manipularEspecieNaoCad(EspecieNaoCadastradaException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));

        return new MensagemDeErro(400, erros);
    }

}
