package br.com.zup.ApiPetShop.controllers;

import br.com.zup.ApiPetShop.dtos.AnimalDTO;
import br.com.zup.ApiPetShop.dtos.PesquisaEspecieDTO;
import br.com.zup.ApiPetShop.services.PetShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/petshop")
public class PetShopController {

    @Autowired
    private PetShopService petShopService;

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT) //alterando a resposta do status da resposta da requisição
                                            //neste caso o metodo terá sucesso e não retorna nada ao usuario;
    public void cadastrarPet(@RequestBody @Valid AnimalDTO animalDTO) {
        petShopService.cadastarPet(animalDTO);
    }

    @GetMapping
    public List<AnimalDTO> exibirLista(){
        return petShopService.exibirLista();
    }

    @GetMapping("/pet/dono/{especie}")
    public List<AnimalDTO> resPesquisaEspecie(@PathVariable String especie) {
        return petShopService.exibirMesmaEspecie(especie);
    }

    @GetMapping("/{nome}") //pegando a variavel do caminho
    public AnimalDTO pesquisarPetPeloNome(@PathVariable String nome) { //tras a variavel para o parametro
        return petShopService.pesquisaAnimalPeloNome(nome);
    }

    @GetMapping("/dono/{email}")
    public AnimalDTO pesquisarPetPeloEmailDoDono(@PathVariable String email){
        return petShopService.pesquisaAnimalPeloDono(email);
    }

    //Atualizando pet com o verbo Put
    @PutMapping()
    public AnimalDTO atualizaOuAdicionaPet(@RequestBody @Valid AnimalDTO animalDTO){
        petShopService.atualizaOuCadastraAnimal(animalDTO);
        return animalDTO;
    }

}
