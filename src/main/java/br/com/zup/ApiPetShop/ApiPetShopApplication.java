package br.com.zup.ApiPetShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPetShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPetShopApplication.class, args);
	}

}
