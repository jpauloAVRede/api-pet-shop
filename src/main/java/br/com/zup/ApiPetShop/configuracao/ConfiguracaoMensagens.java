package br.com.zup.ApiPetShop.configuracao;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Component //essa classe será um componente do nosso sistema; pega a classe e instancia ela para o Spring
public class ConfiguracaoMensagens {

    @Bean //significa para o Spring executar esse metodo e pega objeto e guarda como configuração
            //coloca na lista de arquivos gerenciaveis pelo Spring;
    public MessageSource messageResource(){
        //essa classe é pra dizer onde estão localizada a mensagem
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:MensagensDeValidacao"); //nome do arquivo que centralizamos as mensagens de validação
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    //precisa criar outro método que retornará qual a localização para validação do factor bean
    @Bean
    public LocalValidatorFactoryBean validator(){
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageResource());
        return bean;
    }

}
