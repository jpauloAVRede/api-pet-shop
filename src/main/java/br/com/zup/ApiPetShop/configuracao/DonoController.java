package br.com.zup.ApiPetShop.configuracao;

import br.com.zup.ApiPetShop.dtos.DonoDTO;
import br.com.zup.ApiPetShop.services.DonoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/dono/")
public class DonoController {

    @Autowired
    private DonoService donoService;

    //boa pratica no Http... endPoint amigável e não agressivos;
        //deve mapear todos os recursos especifico, com o nome desse dominio que ele pertence (@RequesteMAapping("/dono"))
        //quando o recuso do endPoint é obter mais de um objeto, deve-se colocar o endPoint no plural: "/donos"
        //não deve existir barra no final do endPoint: ex:. "/donos/"

    @PostMapping
    public DonoDTO cadastrarDono(@RequestBody @Valid DonoDTO donoDTO){
        return donoService.adicionarDono(donoDTO);
    }

}
